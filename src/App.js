import React, { Component,Fragment} from 'react';
import {Route,Switch,withRouter} from "react-router-dom";
import {NotificationContainer} from 'react-notifications';
import {connect} from "react-redux";

import Toolbar from "../src/components/UI/Toolbar/Toolbar";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register"
import Gallery from "./containers/Gallery/Gallery"
import AddNewPhoto from "./containers/AddPhoto/AddNewPhoto";

import {logoutUser} from "./store/actions/userActions";


class App extends Component {
  render() {
    return (
      <Fragment>
        <header><Toolbar user={this.props.user} logout={this.props.logout}/></header>
          <NotificationContainer/>
          <Switch>
            <Route path="/" exact component={Gallery}/>
            <Route path="/gallery/add_new" exact component={AddNewPhoto}/>

            <Route path="/users/:id" exact component={Gallery}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
          </Switch>
      </Fragment>
    );
  }
}

const mapStateToProps = state =>({
  user: state.users.user
});
const mapDispatchToProps = dispatch =>({
  logout: () => dispatch(logoutUser())
});
export default  withRouter(connect(mapStateToProps,mapDispatchToProps)(App));
