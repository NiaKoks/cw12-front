import React, {Component,Fragment} from 'react';
import PhotoCard from "../../components/PhotoCard/PhotoCard";
import {connect} from "react-redux";
import {deletePhoto, fetchGallery} from "../../store/actions/galleryActions";

class Gallery extends Component {
    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchGallery(id);
    };
    render (){

        if (!this.props.gallery) return null;
     return(
         <Fragment>
             {this.props.gallery.map(photo =>(
                 <PhotoCard className="PhotoCard"
                            key={photo._id}
                            _id ={photo._id}
                            title={photo.title}
                            image={photo.image}
                            photoUser={photo.user}
                            user={this.props.user}
                            delete={this.props.deletePhoto}
                 />
             ))}
         </Fragment>
     )
    }
};


const mapStateToProps = state =>{
    return{
        gallery: state.gallery.gallery,
        user: state.users.user
    }
};
const mapDispatchToProps = dispatch =>{
    return{
        fetchGallery: () => dispatch(fetchGallery()),
        deletePhoto: (id,photoID) => dispatch(deletePhoto(id,photoID))
    }
};
export default connect(mapStateToProps,mapDispatchToProps)(Gallery);