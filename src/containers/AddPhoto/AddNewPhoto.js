import React, {Component,Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {submitPhoto} from "../../store/actions/galleryActions";


class AddNewPhoto extends Component {
    state={
      image:'',
      title:'',
      info:''
    };
    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };
    submitFormHandler = event =>{
      event.preventDefault();
        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        this.postPhoto(formData);
    };
    postPhoto = photoData => {
        this.props.onPhotoSubmitted(photoData).then(() => {
            this.props.history.push('/');
        });
    };
    render() {
        return (
            <Fragment>
                <h3>Submit new Photo</h3>
                <Form onSubmit={this.submitFormHandler}>
                    <FormGroup>
                        <Label sm={2} for="title">Title:</Label>
                        <Col sm={10}>
                           <Input type="text" name="title" id="name"
                                  placeholder="Input title of the photo"
                                  value={this.state.title}
                                  onChange={this.inputChangeHandler}
                           />
                        </Col>
                    </FormGroup>
                    <FormGroup>
                        <Label sm={2} for="info">Description: (optional)</Label>
                        <Col sm={10}>
                            <Input type="text" name="info" id="info"
                                   placeholder="Type some description to photo,if you want"
                                   value={this.state.info}
                                   onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup>
                        <Label sm={2} for="image">Image:</Label>
                        <Col sm={10}>
                            <Input type="file" name="image" id="image"
                                   onChange={this.fileChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup>
                        <Col>
                            <Button type="submit" color="primary">Submit</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}
const mapDispatchToProps = dispatch =>({
   onPhotoSubmitted: photoData => dispatch(submitPhoto(photoData))
});
export default connect(null,mapDispatchToProps)(AddNewPhoto);