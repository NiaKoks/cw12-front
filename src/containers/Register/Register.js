import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, FormGroup, Form, Label, Input} from "reactstrap";
import {registerUser} from "../../store/actions/userActions";
import {connect} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Register extends Component {
    state = {
        username: '',
        password: '',
        displayName: '',
        avatar: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.registerUser({...this.state});
    };

    getFieldError = fieldName => {
        return this.props.error && this.props.error.errors
                    && this.props.error.errors[fieldName]
                    && this.props.error.errors[fieldName].message;
    };

    render() {
        return (
            <Fragment>
                <h2>Register new user</h2>
                {this.props.error && this.props.error.global && (
                    <Alert color="danger">
                        {this.props.error.global}
                    </Alert>
                )}
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="username"
                        title="Username"
                        type="text"
                        value={this.state.username}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('username')}
                        placeholder="Enter your desire username"
                        autoComplete="new-username"
                    />
                    <FormElement
                        propertyName="password"
                        title="password"
                        type="password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('password')}
                        placeholder="Enter your desire password"
                        autoComplete="new-password"
                    />
                    <FormElement
                        propertyName="displayName"
                        title="displayName"
                        type="displayName"
                        value={this.state.displayName}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('displayName')}
                        placeholder="Enter your desire Name that will be visible on the site"
                        autoComplete="new-displayName"
                    />
                    <FormGroup row>
                        <Label sm={2} for="avatar">Avatar:</Label>
                        <Col sm={10}>
                            <Input
                                type="url"
                                name="avatar" id="avatar"
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">
                                Register
                            </Button>
                        </Col>
                    </FormGroup>
                    <FormGroup>
                        <FacebookLogin/>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);