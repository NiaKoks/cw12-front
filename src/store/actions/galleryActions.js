import axios from "../../axios-api";

export const FETCH_GALLERY_SUCCESS = "FETCH_GALLERY_SUCCESS";
export const FETCH_HISTORY_SUCCESS = "FETCH_HISTORY_SUCCESS";
export const SUBMIT_TO_GALLERY_SUCCESS = "SUBMIT_TO_GALLERY_SUCCESS";

export const fetchGallerySuccess = images => ({type:FETCH_GALLERY_SUCCESS, images});
export const fetchHistorySuccess = galleryImage =>({type:FETCH_HISTORY_SUCCESS,galleryImage});
export const submitToGallerySuccess = galleryImage =>({type:SUBMIT_TO_GALLERY_SUCCESS,galleryImage})

export const fetchGallery = () =>{
  return dispatch =>{
      return axios.get('/gallery').then(response => dispatch(fetchGallerySuccess(response.data)))
  };
};

export const submitPhoto = photoData =>{
  return dispatch=>{
      return axios.post('/gallery',photoData).then(()=>dispatch(submitToGallerySuccess()))
  }
};

export const deletePhoto =(id,photoID) =>{
  return dispatch =>{
      return axios.delete(`/gallery/${id}`).then(()=>dispatch(fetchGallery(photoID)))
  }
};
