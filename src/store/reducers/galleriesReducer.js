import {FETCH_GALLERY_SUCCESS} from "../actions/galleryActions";

const initialState ={
    gallery:[]
};


const galleriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_GALLERY_SUCCESS:
            return{...state,gallery: action.images}
        default:
            return state;
    }
};

export default galleriesReducer;