import React from 'react'
import Popup from 'reactjs-popup'
import PropTypes from 'prop-types';
import Thumbnail from "../../Thumbnail/Thumbnail";
import {apiURL} from "../../../constants";

const PopUp = props => (
    <Popup trigger={ <div><Thumbnail image={props.image}/></div>}
           on="click" position='center center' modal >
                <img src={apiURL + '/uploads/' + props.image}/>
    </Popup>
);
PopUp.propTypes={
    image: PropTypes.any
};


export default PopUp;