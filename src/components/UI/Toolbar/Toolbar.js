import React ,{Fragment} from 'react';
import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavItem,
    NavLink,
    UncontrolledDropdown
} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';
import FacebookLogin from "../../FacebookLogin/FacebookLogin";
import "./Toolbar.css"
const Toolbar = ({user,logout}) => {
    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand tag={RouterNavLink} to="/">Photo Gallery</NavbarBrand>

            <Nav className="ml-auto" navbar>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/" exact>Home</NavLink>
                </NavItem>

                {user ? (
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                            <div className="avatar">
                                <img src={user.avatar} alt=""/>
                            </div>
                            Hello, {user.displayName}!
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem>
                                <NavLink tag={RouterNavLink} to="/gallery/add_new">
                                    Add New Photo
                                </NavLink>
                            </DropdownItem>
                            <DropdownItem>
                                <NavLink tag={RouterNavLink} to="/users/:id">
                                    My Gallery
                                </NavLink>
                            </DropdownItem>
                            <DropdownItem divider />
                            <DropdownItem onClick={logout}>
                                Logout
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                ) :(
                    <Fragment>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/register" exact>Sign Up</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/login" exact>Login</NavLink>
                        </NavItem>
                        <FacebookLogin/>
                    </Fragment>
                )}
            </Nav>
        </Navbar>
    );
};

export default Toolbar;