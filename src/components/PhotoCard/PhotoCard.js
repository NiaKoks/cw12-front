import React ,{Fragment} from 'react';
import PropTypes from 'prop-types';
import {Button, Card, CardBody,CardSubtitle, CardText, CardTitle} from "reactstrap"
import './PhotoCard.css'
import PopUp from "../UI/PopUp/PopUp";
const PhotoCard = props => {
    return (
        <Card>
            <CardBody>
              <CardTitle>{props.title}</CardTitle>
                <PopUp image={props.image}/>
              <CardText>{props.info}</CardText>
                {props.user && props.user.displayName === props.photoUser.displayName &&
                <Fragment>
                    <Button color="danger" onClick={event=> { event.stopPropagation(); props.delete(props._id)}}>
                        Delete
                    </Button>
                </Fragment>
                }
                <CardSubtitle><p>By: {props.photoUser.displayName}</p></CardSubtitle>
            </CardBody>
        </Card>
    );
};

PhotoCard.propTypes={
  user: PropTypes.object.isRequired,
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  info: PropTypes.string
};

export default PhotoCard;